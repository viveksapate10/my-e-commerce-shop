import { useDispatch, useSelector } from "react-redux"
import { Button } from "react-bootstrap"
import { Card } from "react-bootstrap"
import { remove } from "../store/cartSlice"

const Cart=()=>{

const products=useSelector(state=>state.cart)

const dispatch=useDispatch()

const removeToCard= (id)=>{

    dispatch(remove(id))
}

const cards = products.map(Product=>(

    <div className="col-md-12" style={{marginBottom:'10px'}}>
            <Card key={Product.id} className="h-100">
    
        <div className="text-centre">
        <Card.Img variant="top" src={Product.image}
          style={{width:'100px',height:'130px'}} />
        </div>
          
          <Card.Body>
            <Card.Title>{Product.title}</Card.Title>
            <Card.Text>{Product.price}</Card.Text>
           
          </Card.Body>
          <Card.Footer>
          <Button variant="danger" onClick={()=>removeToCard(Product.id)}> Remove item from cart  </Button>
          </Card.Footer>
           </Card>
        </div>
    ))
    


    return(
    <>
    <h2>Cart</h2>
    {/*JSON.stringify(productCart)*/}

    <div className="row">
        {cards}
    </div>
    
    </>
    
    )
    
    }
    export default Cart