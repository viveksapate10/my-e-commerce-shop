import { useEffect, useState } from "react"
import  Button  from "react-bootstrap/Button"
import  Card  from "react-bootstrap/Card"
import { useDispatch, useSelector } from "react-redux"
import { add } from "../store/cartSlice"
import { getProducts } from "../store/productSlice"

const Product = () => {

// const[products, getProducts]=useState([])

const dispatch = useDispatch()

const {data : products}=useSelector(state=>state.products)



useEffect(()=>{
//dispatch an action for fetch products from API
dispatch(getProducts())
//call the API here 
/*fetch('https://fakestoreapi.com/products')
.then(data=>data.json())
.then(result=>getProducts(result))*/
},[])



const addToCart=(Product)=>{
  //dispatch action here
  dispatch(add(Product))
}

const cards = products.map(Product=>(

<div className="col-md-3" style={{marginBottom:'10px'}}>
        <Card key={Product.id} className="h-100">

    <div className="text-centre">
    <Card.Img variant="top" src={Product.image}
      style={{width:'100px',height:'130px'}} />
    </div>
      
      <Card.Body>
        <Card.Title>{Product.title}</Card.Title>
        <Card.Text>{Product.price}</Card.Text>
       
      </Card.Body>
      <Card.Footer>
      <Button variant="primary" onClick={()=>addToCart(Product)}> Add to Cart</Button>
      </Card.Footer>
       </Card>
    </div>
))



return(
<>
<h1> Our Products </h1>
<div className="row">
{cards}

</div>



</>

)
}

export default Product