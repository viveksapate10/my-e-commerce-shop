import logo from './logo.svg';
import './App.css';
import Product from './Components/Product';
import 'bootstrap/dist/css/bootstrap.min.css';
import{createBrowserRouter, createRoutesFromElements,Route, RouterProvider} from 'react-router-dom'
import Layout from './Components/Layout';
import Dashboard from './Components/Dashboard';
import Cart from './Components/Cart';

function App() {
  const router= createBrowserRouter(createRoutesFromElements(
    <Route path="/"element={<Layout/>}>
      <Route index element={<Dashboard/>}></Route>
      <Route path="/Cart" element={<Cart/>}></Route> 
    </Route>
  ))

  return (
    <div className="App">

     <RouterProvider router={router}/>

    </div>
  );
}

export default App;
