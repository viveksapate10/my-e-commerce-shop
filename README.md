# Project Tittle-
My E-Commerce Shop

## Description
My E-Commerce Shop is the Frontend  project to adding items to cart and puchase the items. It includes the javascript,HTML,CSS. This project is implemented by using reducer,useDispatch,useSelector,provider Functions.also designed the Frontend (user interface)  using fake API for better experience.


# Developed by-
Vivek Mahaveer Sapate
Email:viveksapate10@gmail.com
Phone:+91-7411548299

## Getting Started

### Installing

* Download this source file (https://gitlab.com/viveksapate10/my-e-commerce-shop.git) into local machine and Open in visual studio code.


### Dependencies
Packages ,that are already installed in this project

*Redux Toolkit

*Bootstrap

*Router DOM

Note-No need to install other packages to run this project.

### Executing program

* After opening this project go to >>View >>Terminal

* After Opening the Terminal ,write a command>> **npm start** <<>>


# Challenges Faced
*Difficulty to Arrange the page for different dimensions.

## Help

Any advise for common problems or issues.
command to run if program contains helper info

# FAQ's
* Do you have a data store?
ANS-No Data Store.

* What is left?
ANS-Payment gateway is not added
